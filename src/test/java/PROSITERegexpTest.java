import PROSITE.Fit;
import PROSITE.PrositeRegexp;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PROSITERegexpTest {

    @Test
    public void test_singleLetter_pass(){
        PrositeRegexp regexp = new PrositeRegexp("G");

        String sequence = "GGG";
        List<Fit> actual = regexp.match(sequence);

        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("G", 0, sequence));
        expected.add(new Fit("G", 1, sequence));
        expected.add(new Fit("G", 2, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_anyOfSet_pass(){
        PrositeRegexp regexp = new PrositeRegexp("[AB]");


        String sequence = "ACB";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("A", 0, sequence));
        expected.add(new Fit("B", 2, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_notFromSet_pass(){
        PrositeRegexp regexp = new PrositeRegexp("{AB}");


        String sequence = "ACB";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("C", 1, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_anyAfterA_pass(){
        PrositeRegexp regexp = new PrositeRegexp("A-x");


        String sequence = "ACAAB";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("AC", 0, sequence));
        expected.add(new Fit("AA", 2, sequence));
        expected.add(new Fit("AB", 3, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_tripleA_pass(){
        PrositeRegexp regexp = new PrositeRegexp("A(3)");


        String sequence = "AAABAAA";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("AAA", 0, sequence));
        expected.add(new Fit("AAA", 4, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_tripleAny_pass(){
        PrositeRegexp regexp = new PrositeRegexp("x(3)");


        String sequence = "ABCAB";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("ABC", 0, sequence));
        expected.add(new Fit("BCA", 1, sequence));
        expected.add(new Fit("CAB", 2, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_doubleB_pass(){
        PrositeRegexp regexp = new PrositeRegexp("B(2)");


        String sequence = "BBBCBB";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("BB", 0, sequence));
        expected.add(new Fit("BB", 1, sequence));
        expected.add(new Fit("BB", 4, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_BFromTwoToThree_pass(){
        PrositeRegexp regexp = new PrositeRegexp("B(2,3)");


        String sequence = "BBBCBB";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("BB", 0, sequence));
        expected.add(new Fit("BBB", 0, sequence));
        expected.add(new Fit("BB", 1, sequence));
        expected.add(new Fit("BB", 4, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void test_integration_pass(){
        PrositeRegexp regexp = new PrositeRegexp("B(2,3)-x-A(2)-[AB](1,2)-{AB}");


        String sequence = "BBKAABACGBBBKAAABX";
        List<Fit> actual = regexp.match(sequence);


        List<Fit> expected = new ArrayList<>();
        expected.add(new Fit("BBKAABAC", 0, sequence));
        expected.add(new Fit("BBBKAAABX", 9, sequence));
        expected.add(new Fit("BBKAAABX", 10, sequence));
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RuntimeException.class)
    public void test_noClosingParenthesis_throwException(){
        PrositeRegexp regexp = new PrositeRegexp("B(2,3)-x-A(2-[AB](1,2)-{AB}");
    }

    @Test(expected = RuntimeException.class)
    public void test_noHyphen_throwException(){
        PrositeRegexp regexp = new PrositeRegexp("B(2,3)x-A(2-[AB](1,2)-{AB}");
    }

}
