package PROSITE;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Fit {
    private String sequence;
    private int index;
    private String baseSequence;

    public void print(){
        System.out.println(baseSequence.substring(0, index) + "(" + sequence + ")" + baseSequence.substring(index + sequence.length()));
    }
}
