package PROSITE;

import PROSITE.regexpElements.RegexpElement;
import PROSITE.regexpElements.RegexpElementsFactory;

import java.util.ArrayList;
import java.util.List;

public class PrositeRegexp {

    private List<RegexpElement> elements = new ArrayList<>();

    public PrositeRegexp(String regexp){
        String[] tokens = regexp.split("-");

        for(String token : tokens){
            elements.add(RegexpElementsFactory.produce(token));
        }

    }

    public List<Fit> match(String sequence){
        List<Fit> matching = new ArrayList<>();
        for(int i = 0; i < sequence.length(); i++){
            matching.addAll(getAllMatchingAtIndex(sequence, i));
        }
        return matching;
    }

    private List<Fit> getAllMatchingAtIndex(String sequence, int index) {
        List<Fit> result = new ArrayList<>();
        result.addAll(getAllMatchingAtIndex(sequence, index, index, 0, ""));
        return result;
    }

    private List<Fit> getAllMatchingAtIndex(String sequence, int startIndex, int currentIndex, int elementIndex, String currentFit){
        List<Fit> result = new ArrayList<>();
        if(elementIndex >= elements.size()){
            result.add(new Fit(currentFit, startIndex, sequence));
        } else {
            RegexpElement currentElement = elements.get(elementIndex);
            for (int j = currentElement.getMinRepetition(); j <= currentElement.getMaxRepetition(); j++) {
                if(currentIndex + j > sequence.length()){
                    return result;
                }
                String newFit = currentFit + sequence.substring(currentIndex, currentIndex+j);
                if(currentElement.match(sequence.substring(currentIndex, currentIndex+j), j)) {
                    result.addAll(getAllMatchingAtIndex(sequence, startIndex, currentIndex + j, elementIndex + 1, newFit));
                }
            }
        }
        return result;
    }

    private boolean shouldBreak(int i, int j, String sequence, int repetitions){
        if(i+j >= sequence.length()){
            return true;
        }
        return !elements.get(j).match(sequence.substring(i + j, i + j + repetitions), repetitions);
    }

}
