package PROSITE;

import java.util.ArrayList;
import java.util.List;

public class AminoAcidsCodes {

    public static List<Character> codes = new ArrayList<>();

    static {
        String allCodesString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for(char code : allCodesString.toCharArray()){
            codes.add(code);
        }
    }

    public static boolean validateCode(Character code){
        return codes.contains(code);
    }

}
