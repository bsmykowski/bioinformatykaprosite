package PROSITE;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        PrositeRegexp regexp = new PrositeRegexp("[RK]-G(3)-{EDRKHPCG}(2)-[AGSCI]-[FY]-[LIVA]-x-[FYM]");

        String sequence = "KLTGRPRGGGVVAFVRYRGVAFVRYNKREKGGGABSYIHMEAQ";
        List<Fit> matches = regexp.match(sequence);

        System.out.println(sequence);
        for(Fit match : matches){
            match.print();
        }

    }

}
