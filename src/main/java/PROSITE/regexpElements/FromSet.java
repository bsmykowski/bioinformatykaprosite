package PROSITE.regexpElements;

import PROSITE.Range;

import java.util.Set;

public class FromSet extends RegexpElement {

    private Set<Character> setOfLetters;

    public FromSet(Range range, Set<Character> setOfLetters){
        super(range);
        this.setOfLetters = setOfLetters;
    }

    @Override
    public boolean match(Character aminoAcidCode) {
        return setOfLetters.contains(aminoAcidCode);
    }
}
