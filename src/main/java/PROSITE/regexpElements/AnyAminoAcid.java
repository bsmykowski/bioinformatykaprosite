package PROSITE.regexpElements;

import PROSITE.AminoAcidsCodes;
import PROSITE.Range;

public class AnyAminoAcid extends RegexpElement {

    public AnyAminoAcid(Range range) {
        super(range);
    }

    @Override
    public boolean match(Character aminoAcidCode) {
        return AminoAcidsCodes.codes.contains(aminoAcidCode);
    }
}
