package PROSITE.regexpElements;

import PROSITE.Range;

public class ExactAminoAcid extends RegexpElement {

    private Character aminoAcidCode;

    public ExactAminoAcid(Range range, Character aminoAcidCode){
        super(range);
        this.aminoAcidCode = aminoAcidCode;
    }

    @Override
    public boolean match(Character aminoAcidCode) {
        return aminoAcidCode.equals(this.aminoAcidCode);
    }
}
