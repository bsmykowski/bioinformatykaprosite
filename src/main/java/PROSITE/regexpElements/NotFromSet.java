package PROSITE.regexpElements;

import PROSITE.AminoAcidsCodes;
import PROSITE.Range;

import java.util.Set;

public class NotFromSet extends RegexpElement {

    private Set<Character> setOfLetters;

    public NotFromSet(Range range, Set<Character> setOfLetters){
        super(range);
        this.setOfLetters = setOfLetters;
    }

    @Override
    public boolean match(Character aminoAcidCode) {
        return !setOfLetters.contains(aminoAcidCode) && AminoAcidsCodes.validateCode(aminoAcidCode);
    }
}
