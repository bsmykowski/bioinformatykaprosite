package PROSITE.regexpElements;


import PROSITE.AminoAcidsCodes;
import PROSITE.Range;

import java.util.HashSet;
import java.util.Set;

public class RegexpElementsFactory {

    public static RegexpElement produce(String elementDataRaw){

        if (elementDataRaw.length() == 0){
            throw new RuntimeException("Empty regexp element data.");
        }

        Range repetitionRange = getRepetitionRange(elementDataRaw);
        String elementData = getElementDataWithoutRepetition(elementDataRaw);

        if(isExactAminoAcidElement(elementData)) {
            char aminoAcidCode = elementData.charAt(0);
            return new ExactAminoAcid(repetitionRange, aminoAcidCode);
        } else if (isFromSetElement(elementData)){
            return new FromSet(repetitionRange, getAminoAcidsCodesSet(elementData));
        } else if (isNotFromSetElement(elementData)){
            return new NotFromSet(repetitionRange, getAminoAcidsCodesSet(elementData));
        } else if (isAnyAminoAcidElement(elementData)){
            return new AnyAminoAcid(repetitionRange);
        }

        throw new RuntimeException("Couldn't parse regexp element: " + elementDataRaw);
    }

    private static boolean isAnyAminoAcidElement(String elementData) {
        return elementData.length() == 1 && elementData.charAt(0) == 'x';
    }

    private static boolean isNotFromSetElement(String elementData) {
        return elementData.charAt(0) == '{' && elementData.charAt(elementData.length()-1) == '}';
    }

    private static boolean isFromSetElement(String elementData) {
        return elementData.charAt(0) == '[' && elementData.charAt(elementData.length()-1) == ']';
    }

    private static boolean isExactAminoAcidElement(String elementData) {
        return elementData.length() == 1 && AminoAcidsCodes.validateCode(elementData.charAt(0));
    }

    private static String getElementDataWithoutRepetition(String elementData) {
        if(elementData.charAt(elementData.length()-1) == ')') {
            int lastOpeningParenthesisIndex = elementData.lastIndexOf('(');
            if (lastOpeningParenthesisIndex == -1) {
                throw new RuntimeException("Opening parenthesis matching to closing parenthesis not found: " + elementData);
            }
            return elementData.substring(0, lastOpeningParenthesisIndex);
        }
        return elementData;
    }

    private static Range getRepetitionRange(String elementData) {
        int minRepetition = 1;
        int maxRepetition = 1;
        if(elementData.charAt(elementData.length()-1) == ')'){
            int lastOpeningParenthesisIndex = elementData.lastIndexOf('(');
            if(lastOpeningParenthesisIndex == -1){
                throw new RuntimeException("Opening parenthesis matching to closing parenthesis not found: " + elementData);
            }
            String repetitionString = elementData.substring(lastOpeningParenthesisIndex+1, elementData.length()-1);
            if(repetitionString.contains(",")){
                String[] minMaxTokens = repetitionString.split(",");
                if(minMaxTokens.length != 2){
                    throw new RuntimeException("Wrong amount of repetition arguments: " + elementData);
                }
                minRepetition = Integer.parseInt(minMaxTokens[0]);
                maxRepetition = Integer.parseInt(minMaxTokens[1]);
            } else {
                minRepetition = Integer.parseInt(repetitionString);
                maxRepetition = minRepetition;
            }
        }

        return new Range(minRepetition, maxRepetition);
    }

    private static Set<Character> getAminoAcidsCodesSet(String elementData) {
        Set<Character> matchingSet = new HashSet<>();
        for (int i = 1; i < elementData.length() - 1; i++) {
            if (AminoAcidsCodes.validateCode(elementData.charAt(i))) {
                matchingSet.add(elementData.charAt(i));
            } else {
                throw new RuntimeException("Invalid amino acid code in set: " + elementData);
            }
        }
        return matchingSet;
    }

}
