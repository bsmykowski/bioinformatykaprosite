package PROSITE.regexpElements;

import PROSITE.Range;
import lombok.Data;

@Data
public abstract class RegexpElement {
    private Range range;

    public RegexpElement(Range range){
        this.range = range;
    }

    public boolean match(String aminoAcidCodes, int repetition){
        if(repetition < range.getMin() || repetition > range.getMax()){
            return false;
        }
        for(int i = 0; i < repetition; i++){
            if (!match(aminoAcidCodes.charAt(i))){
                return false;
            }
        }
        return true;
    }

    protected abstract boolean match(Character aminoAcidCode);

    public int getMinRepetition() {
        return range.getMin();
    }

    public int getMaxRepetition() {
        return range.getMax();
    }
}
